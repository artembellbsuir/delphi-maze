unit Main;

interface

uses
   Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
   System.Classes, Vcl.Graphics,
   Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
   MazeUnit, Vcl.ExtCtrls, Vcl.StdCtrls;

type
   THero = record
      Pos: TPoint;
      Health: Integer;
   end;

   TMainForm = class(TForm)
      lblHint: TLabel;
      lblInfo: TLabel;
      Timer: TTimer;
      procedure TimerTimer(Sender: TObject);
   published
      Field: TImage;
      procedure FormCreate(Sender: TObject);
      procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
   public
      procedure ClearField();
      procedure DrawFloor(const Floor: TFloor);
      procedure DrawCell(const I, J, X, Y, Code: Integer);
      procedure DrawWall(const X, Y, Dest: Integer);
      procedure DrawHero();
      procedure DrawFog();

      procedure InitGame();
      procedure InitHero();
      procedure MoveHero(const Dest: Integer);
      procedure CheckCurrentPosition();
      procedure ShowTip();
      procedure ShowPath(const Number: Integer);
      function LookAroundExceptPrevPos(const ExceptDir: Integer;
        const Dest, Now: TPoint): Integer;

      function IsInElevator(): Boolean;
      // function CanGoDown(): Boolean;
      // function
   end;

var
   MainForm: TMainForm;
   Maze: TMaze;

implementation

{$R *.dfm}

const
   Colors: array [0 .. 2] of TColor = (clRed, clGreen, clBlue);

var
   CellSize, CurFloor: Integer;
   CurX, CurY, FogX, FogY: Integer;
   Hero: THero;
   Heart, Trap: TBitmap;

const
   HeroRadius = 10;
   UP_DIRECTION = 1;
   RIGHT_DIRECTION = 2;
   DOWN_DIRECTION = 4;
   LEFT_DIRECTION = 8;

procedure TMainForm.FormCreate(Sender: TObject);
begin
   InitGame();
end;

procedure TMainForm.InitGame;
begin
   Maze := TMaze.Create();
   CurFloor := 0;

   Trap := TBitmap.Create();
   Trap.LoadFromFile('trap2.bmp');
   Trap.Transparent := True;
   Trap.TransparentColor := clBlack;

   Heart := TBitmap.Create();
   Heart.LoadFromFile('heart.bmp');
   Heart.Transparent := True;
   Heart.TransparentColor := clBlack;

   InitHero();
   // InitStuff();
   lblHint.Caption := '��������: ' + IntToStr(Hero.Health);

   DrawFloor(Maze.Floors[0]);
end;

procedure TMainForm.InitHero;
begin
   with Hero do
   begin
      Pos.X := 0;
      Pos.Y := 0;
      Health := 3;
   end;
end;

function TMainForm.IsInElevator: Boolean;
begin
   Result := Maze.Stuff[CurFloor, Hero.Pos.Y, Hero.Pos.X] = 3;
end;

function TMainForm.LookAroundExceptPrevPos(const ExceptDir: Integer;
  const Dest, Now: TPoint): Integer;
var
   CurCode: Integer;
   Flag: Boolean;
begin
   //
   if Now = Dest then
   begin
      ShowMessage('Found');
      Result := 1;
   end
   else
   begin
      CurCode := Maze.Floors[CurFloor, Now.Y, Now.X];
      Flag := False;

      if ((CurCode and UP_DIRECTION = UP_DIRECTION) and (UP_DIRECTION <> ExceptDir)) then
         LookAroundExceptPrevPos(DOWN_DIRECTION, Dest, TPoint.Create(Now.X, Now.Y - 1));

      if ((CurCode and RIGHT_DIRECTION = RIGHT_DIRECTION) and (RIGHT_DIRECTION <> ExceptDir)) then
         LookAroundExceptPrevPos(LEFT_DIRECTION, Dest, TPoint.Create(Now.X, Now.Y - 1));

      if ((CurCode and DOWN_DIRECTION = DOWN_DIRECTION) and (DOWN_DIRECTION <> ExceptDir)) then
         LookAroundExceptPrevPos(UP_DIRECTION, Dest, TPoint.Create(Now.X, Now.Y - 1));

      if ((CurCode and LEFT_DIRECTION = LEFT_DIRECTION) and (LEFT_DIRECTION <> ExceptDir)) then
         LookAroundExceptPrevPos(RIGHT_DIRECTION, Dest, TPoint.Create(Now.X, Now.Y - 1));
   end;
end;

procedure TMainForm.MoveHero(const Dest: Integer);
var
   CurCode: Integer;
begin
   CurCode := Maze.Floors[CurFloor][Hero.Pos.Y, Hero.Pos.X];
   if (CurCode and Dest = Dest) then
   begin
      case Dest of
         UP_DIRECTION:
            Hero.Pos := TPoint.Create(Hero.Pos.X, Hero.Pos.Y - 1);
         RIGHT_DIRECTION:
            Hero.Pos := TPoint.Create(Hero.Pos.X + 1, Hero.Pos.Y);
         DOWN_DIRECTION:
            Hero.Pos := TPoint.Create(Hero.Pos.X, Hero.Pos.Y + 1);
         LEFT_DIRECTION:
            Hero.Pos := TPoint.Create(Hero.Pos.X - 1, Hero.Pos.Y);
      end;
      ClearField();
      DrawFloor(Maze.Floors[CurFloor]);

      CheckCurrentPosition();
   end
   // else
   // ShowMessage('STAY');

end;

procedure TMainForm.ShowPath(const Number: Integer);
var
   Dest: TPoint;
begin
   //
   // ShowMessage(IntToStr(Number));

   case Number of
      0:
         Dest := TPoint.Create(5, 5);
      1:
         Dest := TPoint.Create(5, 0);
      2:
         Dest := TPoint.Create(0, 4);
   end;

   LookAroundExceptPrevPos(LEFT_DIRECTION, Dest, Hero.Pos);
end;

procedure TMainForm.ShowTip;
var
   Template: string;
   Code: Integer;
begin
   //
   Code := Maze.Floors[CurFloor, Hero.Pos.Y, Hero.Pos.X];
   Template := '��� �������, ��� ����� ������';
   if (Code and UP_DIRECTION = UP_DIRECTION) then
      Template := Template + ' � �����';
   if (Code and RIGHT_DIRECTION = RIGHT_DIRECTION) then
      Template := Template + ' � ������';
   if (Code and DOWN_DIRECTION = DOWN_DIRECTION) then
      Template := Template + ' � ����';
   if (Code and LEFT_DIRECTION = LEFT_DIRECTION) then
      Template := Template + ' � �����';
   lblInfo.Caption := Template + '!';
end;

procedure TMainForm.TimerTimer(Sender: TObject);
begin
   //
//   Maze.OpenCell(CurFloor);
//   DrawFloor(Maze.Floors[CurFloor]);
end;

procedure TMainForm.CheckCurrentPosition;
var
   I: Integer;
begin
   lblInfo.Caption := '';

   if (Maze.Stuff[CurFloor, Hero.Pos.Y, Hero.Pos.X] = 1) then
   begin
      lblInfo.Caption := '�� ����� �������������� ��������!';
      Inc(Hero.Health);

      Maze.Stuff[CurFloor, Hero.Pos.Y, Hero.Pos.X] := 0;
   end;

   if (Maze.Stuff[CurFloor, Hero.Pos.Y, Hero.Pos.X] = 2) then
   begin
      lblInfo.Caption := '�� ������� � �������!';
      Dec(Hero.Health);

      Maze.Stuff[CurFloor, Hero.Pos.Y, Hero.Pos.X] := 0;
   end;

   lblHint.Caption := '��������: ' + IntToStr(Hero.Health);

   if (Hero.Health = 0) then
   begin
      ShowMessage('�� ������!');
      InitGame();
   end;

   if (not(Hero.Pos.X in [0 .. 5]) or not(Hero.Pos.X in [0 .. 5])) then
   begin
      ShowMessage('�� ���������!!!');
      InitGame();
   end;

   ClearField();
   DrawFloor(Maze.Floors[CurFloor]);

   // make message
end;

procedure TMainForm.DrawHero;
begin
   with Field.Canvas do
   begin
      Brush.Color := RGB(255, 157, 0);
      Ellipse(Trunc(Hero.Pos.X * CellSize + CellSize / 2 - HeroRadius),
        Trunc(Hero.Pos.Y * CellSize + CellSize / 2 - HeroRadius),
        Trunc(Hero.Pos.X * CellSize + CellSize / 2 + HeroRadius),
        Trunc(Hero.Pos.Y * CellSize + CellSize / 2 + HeroRadius));
   end;
end;

procedure TMainForm.DrawFloor(const Floor: TFloor);
var
   I, J: Integer;
begin
   // image must be square!
   CellSize := Trunc(Field.ClientWidth / FloorSize);

   CurX := 0;
   CurY := 0;

   // Fog.Picture := nil;
   ClearField();

   for I := 0 to FloorSize - 1 do
   begin
      for J := 0 to FloorSize - 1 do
      begin
         DrawCell(I, J, CurX, CurY, Floor[I, J]);
         CurX := CurX + CellSize;
      end;
      CurY := CurY + CellSize;
      CurX := 0;
   end;

   // DrawStuff();
   DrawHero();
   DrawFog();



end;

procedure TMainForm.DrawFog;
var
   I, J: Integer;
begin
   // CellSize := CellSize - 10;
   // Fog.Picture := nil;
   FogX := 0;
   FogY := 0;

   with Field.Canvas do
   begin
      // Pen.Style := psClear;
      Pen.Color := clGray;
      Pen.Style := psSolid;
      Brush.Color := clGray;
      for I := 0 to FloorSize - 1 do
      begin
         for J := 0 to FloorSize - 1 do
         begin

//            if (Maze.Fog[CurFloor, I, J] = 1) then
//               Rectangle(FogX, FogY, FogX + CellSize, FogY + CellSize);
//
//            FogX := FogX + CellSize;
         end;
         FogY := FogY + CellSize;
         FogX := 0;
      end;

   end;

end;

procedure TMainForm.DrawCell(const I, J, X, Y, Code: Integer);
const
   StuffSize = 10;
begin
   with Field.Canvas do
   begin

      Pen.Color := RGB(214, 214, 214);
      // Pen.Style := psClear;
      Pen.Style := psSolid;
      Pen.Width := 1;
      Brush.Color := RGB(214, 214, 214);
      Rectangle(X, Y, X + CellSize, Y + CellSize);

      if (Maze.Stuff[CurFloor, I, J] = 1) then
      begin
//         Brush.Color := clRed;
//         Rectangle(Trunc(J * CellSize + CellSize / 2 - StuffSize / 2),
//           Trunc(I * CellSize + CellSize / 2 - StuffSize / 2),
//           Trunc(J * CellSize + CellSize / 2 + StuffSize / 2),
//           Trunc(I * CellSize + CellSize / 2 + StuffSize / 2));

         Draw(Trunc(X + CellSize / 2 - 15), Trunc(Y + CellSize / 2 - 15), Heart);
      end;

      if (Maze.Stuff[CurFloor, I, J] = 2) then
      begin
//         Brush.Color := clBlack;
//         Rectangle(Trunc(J * CellSize + CellSize / 2 - StuffSize / 2),
//           Trunc(I * CellSize + CellSize / 2 - StuffSize / 2),
//           Trunc(J * CellSize + CellSize / 2 + StuffSize / 2),
//           Trunc(I * CellSize + CellSize / 2 + StuffSize / 2));
         Draw(Trunc(X + CellSize / 2 - 15), Trunc(Y + CellSize / 2 - 15), Trap);
      end;

      if ((Maze.Stuff[CurFloor, I, J] = 3) or (Maze.Stuff[CurFloor, I, J] = 4))
      then
      begin
         Font.Name := 'Tahoma';
         Font.Size := 20;
         Brush.Style := bsClear;
         TextOut(Trunc(J * CellSize + CellSize / 2 - StuffSize),
           Trunc(I * CellSize + CellSize / 6), 'E');
      end;

      if (Code and UP_DIRECTION <> UP_DIRECTION) then
         DrawWall(X, Y, UP_DIRECTION);

      if (Code and RIGHT_DIRECTION <> RIGHT_DIRECTION) then
         DrawWall(X, Y, RIGHT_DIRECTION);

      if (Code and DOWN_DIRECTION <> DOWN_DIRECTION) then
         DrawWall(X, Y, DOWN_DIRECTION);

      if (Code and LEFT_DIRECTION <> LEFT_DIRECTION) then
         DrawWall(X, Y, LEFT_DIRECTION);

   end;
end;

procedure TMainForm.DrawWall(const X, Y, Dest: Integer);
const
   WallWidth = 6;
begin
   with Field.Canvas do
   begin
      Pen.Style := psSolid;
      Pen.Color := Colors[CurFloor];
      Brush.Color := Colors[CurFloor];
      case Dest of
         UP_DIRECTION:
            Rectangle(X, Y, X + CellSize, Y + WallWidth);
         RIGHT_DIRECTION:
            Rectangle(X + CellSize - WallWidth, Y, X + CellSize, Y + CellSize);
         DOWN_DIRECTION:
            Rectangle(X, Y + CellSize - WallWidth, X + CellSize, Y + CellSize);
         LEFT_DIRECTION:
            Rectangle(X, Y, X + WallWidth, Y + CellSize);
      end;
   end;
end;

procedure TMainForm.ClearField;
begin
   with Field.Canvas do
   begin
      Pen.Style := psClear;
      Brush.Color := clWhite;
      Rectangle(0, 0, Field.ClientWidth, Field.ClientHeight);
   end;
end;

procedure TMainForm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   case Key of
      VK_SPACE:
         begin
            // ShowMessage(IntToStr(Hero.Pos.X) + ' ' + IntToStr(Hero.Pos.Y));
            if (IsInElevator()) then
            begin

               if (CurFloor = 0) then
               begin
//                  lblInfo.Caption := '1';
                  CurFloor := 1;

               end
               else if (CurFloor = 1) then
               begin
//                  lblInfo.Caption := '2';
                  if ((Hero.Pos.X = 5) and (Hero.Pos.Y = 0)) then
                     CurFloor := 2;
                  // ShowMessage('TOP');
                  if ((Hero.Pos.X = 5) and (Hero.Pos.Y = 5)) then
                     // ShowMessage('Bot');
                     CurFloor := 0;
               end
               else if (CurFloor = 2) then
               begin
//                  lblInfo.Caption := '3';
                  CurFloor := 1;

               end;

               // CurFloor := (CurFloor + 1) mod 3;
               // ClearField();
               // DrawFloor(Maze.Floors[CurFloor]);
               ClearField();
               DrawFloor(Maze.Floors[CurFloor]);
            end;
         end;
      VK_UP:
         MoveHero(UP_DIRECTION);
      VK_RIGHT:
         MoveHero(RIGHT_DIRECTION);
      VK_DOWN:
         MoveHero(DOWN_DIRECTION);
      VK_LEFT:
         MoveHero(LEFT_DIRECTION);
      Ord('T'):
         ShowTip();

      Ord('Q'):
         ShowPath(CurFloor);

      VK_NUMPAD8:
         begin
            if (IsInElevator()) and ((CurFloor mod 3) <> 2) then
            begin
               CurFloor := (CurFloor + 1) mod 3;
               ClearField();
               DrawFloor(Maze.Floors[CurFloor]);
            end;
         end;

      VK_NUMPAD2:
         begin
            if (IsInElevator()) and ((CurFloor mod 3) <> 0) then
            begin
               CurFloor := (CurFloor - 1 + 3) mod 3;
               ClearField();
               DrawFloor(Maze.Floors[CurFloor]);
            end;
         end;
   end;
end;

end.
