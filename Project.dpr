program Project;

uses
  Vcl.Forms,
  Main in 'Main.pas' {MainForm},
  MazeUnit in 'MazeUnit.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
