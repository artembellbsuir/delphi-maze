object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'Maze'
  ClientHeight = 403
  ClientWidth = 399
  Color = clBtnFace
  DoubleBuffered = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 120
  TextHeight = 16
  object Field: TImage
    Left = 8
    Top = 48
    Width = 300
    Height = 300
  end
  object lblHint: TLabel
    Left = 8
    Top = 18
    Width = 41
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -20
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lblInfo: TLabel
    Left = 8
    Top = 354
    Width = 6
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -20
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Timer: TTimer
    Interval = 5000
    OnTimer = TimerTimer
    Left = 264
  end
end
