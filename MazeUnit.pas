unit MazeUnit;

interface

const
   FloorSize = 6;


type
   TFloorCell = Integer;
   TFloor = array [0..FloorSize - 1] of array [0..FloorSize - 1] of TFloorCell;
   TBuilding = array of TFloor;

   TMaze = class
      private
         FFloorsCount: Integer;
         FFloorSize: Integer;
         FLifes: Integer;

         FFloors: TBuilding;
         FFog: TBuilding;
         FStuff: TBuilding;  

         procedure InitBuilding();
         procedure InitFloor(var Floor: TFloor);
         procedure InitFog();
         procedure InitStuff();
         
      published
         constructor Create();
         procedure OpenCell(const Index: Integer);

         property Floors: TBuilding read FFloors;
         property Fog: TBuilding read FFog;
         property Stuff: TBuilding read FStuff;
         
         property FloorsCount: Integer read FFloorsCount;
         property FloorsSize: Integer read FFloorSize;
         property Lifes: Integer read FLifes;
         
   end;

implementation

{ Maze }

constructor TMaze.Create;
begin
   with Self do
   begin
      FFloorsCount := 1;
      FLifes := 3;
   end;

   InitBuilding();
   InitStuff();
   InitFog();
end;

procedure TMaze.InitBuilding;
const
   Floor1: TFloor = (
     (2, 12, 6, 8, 6, 12),
     (4, 5, 5, 6, 11, 13),
     (5, 7, 11, 9, 4, 5),
     (7, 11, 12, 6, 13, 5),
     (1, 6, 15, 13, 7, 13),
     (2, 9, 1, 1, 1, 1)
   );

   Floor2: TFloor = (
     (4, 2, 12, 6, 10, 8),
     (3, 10, 15, 15, 14, 12),
     (6, 8, 7, 9, 5, 5),
     (7, 10, 15, 10, 11, 9),
     (3, 8, 7, 12, 6, 12),
     (2, 10, 9, 3, 9, 1)
   );

   Floor3: TFloor = (
     (4, 6, 10, 12, 6, 8),
     (5, 7, 8, 5, 3, 12),
     (3, 13, 6, 13, 6, 9),
     (12, 5, 7, 9, 3, 12),
     (3, 11, 15, 12, 6, 9),
     (2, 10, 9, 3, 11, 8)
   );
var
   i: Integer;
begin
   Randomize;
   SetLength(FFloors, FFloorsCount);

   FFloors[0] := Floor1;
   FFloors[1] := Floor2;
   FFloors[2] := Floor3;
end;

procedure TMaze.InitFloor(var Floor: TFloor);
var
   i, j: Integer;
begin
   // init with our pattern here
   for i := 0 to FFloorSize - 1 do
      for j := 0 to FFloorSize - 1 do
         Floor[i, j] := Random(10);
end;

procedure TMaze.InitFog;
var
   i, j, k: Integer;
begin
   //
   SetLength(FFog, 3);

   for i := 0 to 2 do
      for j := 0 to FloorSize - 1 do
         for k := 0 to FloorSize do
               Fog[i, j, k] := 1;

end;

procedure TMaze.InitStuff;
const
   Stuff1: TFloor = (
      (0, 0, 0, 2, 0, 0),
      (0, 2, 0, 0, 0, 0),
      (0, 0, 0, 0, 1, 0),
      (1, 0, 0, 0, 0, 0),
      (0, 0, 0, 0, 0, 0),
      (0, 0, 0, 1, 2, 3)
   );
   Stuff2: TFloor = (
      (0, 0, 0, 0, 1, 3),
      (0, 0, 2, 0, 0, 0),
      (1, 0, 0, 0, 0, 0),
      (0, 0, 0, 0, 0, 2),
      (0, 2, 0, 1, 0, 0),
      (0, 0, 0, 0, 2, 3)
   );
   Stuff3: TFloor = (
      (0, 0, 0, 0, 0, 3),
      (0, 0, 1, 0, 2, 0),
      (0, 2, 0, 0, 0, 0),
      (2, 0, 0, 1, 0, 0),
      (0, 0, 0, 0, 0, 0),
      (0, 1, 0, 2, 0, 0)
   );
var
   i: Integer;
begin
   SetLength(FStuff, 3);
      
   FStuff[0] := Stuff1;
   FStuff[1] := Stuff2;
   FStuff[2] := Stuff3;
end;

procedure TMaze.OpenCell(const Index: Integer);
begin
   //
   Randomize;

   FFog[Index, Random(FloorSize - 1), Random(FloorSize - 1)] := 0;
end;

end.
